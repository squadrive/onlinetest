package com.example.onlinetest.di

import javax.inject.Scope
import kotlin.annotation.AnnotationTarget.*

@Scope
@Retention(AnnotationRetention.RUNTIME)
@Target(CLASS, FUNCTION, TYPE)
annotation class FragmentScoped
