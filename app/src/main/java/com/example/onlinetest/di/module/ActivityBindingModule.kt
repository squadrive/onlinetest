package com.example.onlinetest.di.module

import com.example.onlinetest.di.ActivityScoped
import com.example.onlinetest.view.main.MainActivity
import com.example.onlinetest.view.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun mainActivity(): MainActivity
}
