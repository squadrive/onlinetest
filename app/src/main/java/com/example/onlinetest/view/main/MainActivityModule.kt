package com.example.onlinetest.view.main

import dagger.Binds
import dagger.Module

@Module
abstract class MainActivityModule {
    @Binds
    abstract fun mainActivityPresenter(impl: MainActivityPresenter): MainActivityContract.Presenter
}
