package com.example.onlinetest.view.main

import com.example.onlinetest.BasePresenter
import com.example.onlinetest.BaseView

interface MainActivityContract {
    interface View: BaseView<Presenter> {

    }

    interface Presenter: BasePresenter<View>{

    }
}