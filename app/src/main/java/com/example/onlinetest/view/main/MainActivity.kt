package com.example.onlinetest.view.main

import android.os.Bundle
import com.example.onlinetest.R
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity: DaggerAppCompatActivity(), MainActivityContract.View {
    @Inject
    lateinit var presenter: MainActivityContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.initPresenter(this)
    }
}